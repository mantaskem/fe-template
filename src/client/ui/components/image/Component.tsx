import * as React from 'react';
import * as classNames from 'classnames';

interface Props {
  src: string;
  className?: string;
}

export const Image: React.FC<Props> = ({ src, className }) => {
  return <img src={src} className={classNames(className)} />;
};
