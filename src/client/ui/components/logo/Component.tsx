import * as React from 'react';

import * as image from 'assets/images/logo.svg';

export function Logo() {
  return <img src={image} className={'p'} alt="logo" />;
}
