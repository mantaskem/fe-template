import * as React from 'react';
import { storiesOf } from '@storybook/react';
import { Button, ButtonType, ButtonSize } from 'ui/components/button';
import { Image } from 'ui/components/image';
import { Logo } from 'ui/components/logo';
import { text } from '@storybook/addon-knobs';

/**
 * COMPONENTS
 */
storiesOf('components/Image', module).add('default', () => (
  <Image src={text('src', 'https://avatars1.githubusercontent.com/u/19638267')} />
));

storiesOf('components/Logo', module).add('default', () => <Logo />);

storiesOf('components/button', module).add('all', () => {
  return (
    <div className="df w60p fww">
      <Button styleType={ButtonType.primary} sizeType={ButtonSize.medium} className="m10">
        Primary Button
      </Button>
      <Button styleType={ButtonType.secondary} sizeType={ButtonSize.medium} className="m10">
        Secondary Button
      </Button>
      <Button styleType={ButtonType.success} sizeType={ButtonSize.medium} className="m10">
        Success Button
      </Button>
      <Button styleType={ButtonType.warning} sizeType={ButtonSize.medium} className="m10">
        Warning Button
      </Button>
      <Button styleType={ButtonType.error} sizeType={ButtonSize.medium} className="m10">
        Error Button
      </Button>
    </div>
  );
});

storiesOf('icons', module).add('default', () => <Logo />);
