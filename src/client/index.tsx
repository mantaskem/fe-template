import * as React from 'react';
import { ConnectedRouter } from 'connected-react-router';
import { createBrowserHistory } from 'history';
import { Provider } from 'react-redux';
import { hydrate } from 'react-dom';
import { configureStore } from 'store/store';
import { App } from 'app';

import 'assets/styles/theme.scss';
// tslint:disable: no-string-literal
const history = createBrowserHistory();
const initialState = window['__INITIAL_STATE__'];

const store = configureStore(history, initialState);

delete window['__INITIAL_STATE__'];

hydrate(
  <Provider store={store}>
    <ConnectedRouter history={history}>
      <App store={store} />
    </ConnectedRouter>
  </Provider>,
  document.getElementById('root')
);

if (process.env.NODE_ENV === 'development' && module.hot) {
  module.hot.accept('app', () => {
    const NewApp = require('app').default;
    hydrate(
      <Provider store={store}>
        <ConnectedRouter history={history}>
          <NewApp store={store} />
        </ConnectedRouter>
      </Provider>,
      document.getElementById('root')
    );
  });
}
