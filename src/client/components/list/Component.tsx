import * as React from 'react';
import ListItem from 'components/list/item/Component';

interface Props {
  ids: string[];
}

const List: React.FC<Props> = ({ ids }) => (
  <div className="w100p df fww fdr jcse acsa aifs h100p">
    {ids.map(id => (
      <ListItem id={id} key={id} />
    ))}
  </div>
);

export default List;
