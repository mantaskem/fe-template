import * as React from 'react';
import OfferCard from 'ui/components/offerCard/Component';

interface Props {
  id: string;
}

const ListItem: React.FC<Props> = ({ id }) => (
  <div className="w320">
    <OfferCard />
  </div>
);

export default ListItem;
