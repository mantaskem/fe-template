import * as React from 'react';

const Footer: React.FC = () => (
  <div className="w100p bg-gray-ft c-white b0 h100">
    <h1>Footer</h1>
  </div>
);

export default Footer;
