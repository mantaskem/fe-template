import * as React from 'react';
import { Button, ButtonSize, ButtonType } from 'ui/components';

interface Props {
  text: string;
  handleClick: () => void;
}

const SimpleButton: React.FC<Props> = ({ text, handleClick }) => (
  <Button sizeType={ButtonSize.medium} styleType={ButtonType.secondary} onClick={handleClick}>
    {text}
  </Button>
);

export default SimpleButton;
