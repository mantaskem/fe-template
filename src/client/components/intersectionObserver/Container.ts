import { connect } from 'react-redux';
import { AppState } from 'store/reducer';
import IntersectionObserver from 'components/intersectionObserver/Component';

interface OwnProps {
  isVisible: boolean;
}

const mapStateToProps = (state: AppState, { isVisible = true }: OwnProps) => ({
  isClientSideLoaded: state.page.isClientSideActive && isVisible
});

export default connect(mapStateToProps)(IntersectionObserver);
