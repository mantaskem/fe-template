import * as React from 'react';
import InView from 'react-intersection-observer';

interface Props {
  isClientSideLoaded: boolean;
  callback: (inView: boolean) => void;
}

const IntersectionObserver: React.FC<Props> = ({ isClientSideLoaded, children, callback }) => {
  if (isClientSideLoaded) {
    return <InView onChange={callback}>{children}</InView>;
  }
  return <>{children}</>;
};

export default IntersectionObserver;
