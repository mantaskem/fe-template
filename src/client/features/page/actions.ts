import { createActionCreator } from 'utils/redux';
import {
  SET_CLIENT_SIDE_ACTIVE,
  SET_SERVER_SIDE_NOT_ACTIVE,
  SET_LANDING_PAGE_SEEN,
  SET_ABOUT_PAGE_SEEN,
  SET_REGISTRATION_PAGE_SEEN
} from 'features/page/constants';

export const setClientSideActive = createActionCreator<boolean>(SET_CLIENT_SIDE_ACTIVE);
export const setServerSideNotActive = createActionCreator<boolean>(SET_SERVER_SIDE_NOT_ACTIVE);

export const setLandingPageSeen = createActionCreator(SET_LANDING_PAGE_SEEN);
export const setAboutPageSeen = createActionCreator(SET_ABOUT_PAGE_SEEN);
export const setRegistrationPageSeen = createActionCreator(SET_REGISTRATION_PAGE_SEEN);
