import { Dispatch } from 'redux';
import { AppState } from 'store/reducer';
import { pouseUserTimeOnApp, startUserTimeOnApp } from 'features/user/timer/thunks';
import { setClientSideActive, setServerSideNotActive } from 'features/page/actions';

export const onPageHidden = () => (dispatch: Dispatch<AppState>) => {
  dispatch(pouseUserTimeOnApp());
};

export const onPageVisible = () => (dispatch: Dispatch<AppState>) => {
  dispatch(startUserTimeOnApp());
};

export const onPageUnload = () => () => {
  console.log('PAGE UNLOAD');
};

export const onClientSideLoaded = () => (dispatch: Dispatch<AppState>) => {
  dispatch(setClientSideActive());
  dispatch(setServerSideNotActive());
};
