import { detectBreakpoint, detectNetworkStatus } from './thunks';
import { AppState } from 'store/reducer';
import { Dispatch } from 'redux';

export const createBreakpointListener = () => (dispatch: Dispatch<AppState>) => {
  window.addEventListener('resize', () => dispatch(detectBreakpoint()));
  dispatch(detectBreakpoint());
};

export const createNetworkConnectionListener = () => (dispatch: Dispatch<AppState>) => {
  window.addEventListener('offline', () => dispatch(detectNetworkStatus()));
  window.addEventListener('online', () => dispatch(detectNetworkStatus()));
  dispatch(detectNetworkStatus());
};
