import { Dispatch } from 'redux';
import { AppState } from 'store/reducer';
import { initializeListeners } from 'features/initializer/listeners';
import { onClientSideLoaded } from 'features/page/thunks';

export const initializeClientSide = () => (dispatch: Dispatch<AppState>) => {
  dispatch(onClientSideLoaded());
  dispatch(initializeListeners());
};
