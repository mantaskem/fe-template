import { getMilisecondsBetweenTwoDates } from 'utils/time';
import { Dispatch } from 'redux';
import { AppState } from 'store/reducer';
import { startTime, setMiliseconds } from 'features/user/timer/actions';

export const startUserTimeOnApp = () => (dispatch: Dispatch<AppState>) => {
  dispatch(startTime(new Date()));
};

export const pouseUserTimeOnApp = () => (
  dispatch: Dispatch<AppState>,
  getState: () => AppState
) => {
  const state = getState();
  const ms = getMilisecondsBetweenTwoDates(new Date(state.user.timer.date));
  dispatch(setMiliseconds(state.user.timer.totalMs + ms));
};
