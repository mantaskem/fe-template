export const getMilisecondsBetweenTwoDates = (date: any) => {
  const now = new Date() as any;
  const diffMs = now - date;

  return diffMs;
};
