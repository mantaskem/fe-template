import { mergeDeepLeft } from 'ramda';

export interface Action<T> {
  type: string;
  payload?: T;
}

export type Reducer<S, P> = (state: S, action: Action<P>) => S;

export const createActionCreator = <P = any>(type: string) => (payload?: P): Action<P> => ({
  type,
  payload
});

interface ReducersMap<S> {
  [actionType: string]: Reducer<S, any>;
}

export const createReducer = <S>(reducersMap: ReducersMap<S>, defaultState: S) => (
  state = defaultState,
  action: Action<any>
): S => (reducersMap.hasOwnProperty(action.type) ? reducersMap[action.type](state, action) : state);

export const set = <T>(_: any, action: Action<T>) => action.payload;

export interface SetByKeyActionPayload<T> {
  key: string;
  value: T;
}

export const setByKey = <S, T extends SetByKeyActionPayload<any> = any>(key: string) => (
  state: S,
  action: Action<T>
): S => (action.payload!.key === key ? action.payload!.value : state);

export const updateByKey = <S, T extends SetByKeyActionPayload<any> = any>(key: string) => (
  state: S,
  action: Action<T>
): S =>
  action.payload!.key === key ? { ...(state as any), ...(action.payload!.value as any) } : state;

export const update = <T>(state: T, action: Action<T>): T => ({
  ...(state as any),
  ...(action.payload as any)
});

export const updateDeep = <T, P>(state: T, action: Action<P>): T & P =>
  mergeDeepLeft(state, action.payload!);
