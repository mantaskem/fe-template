import * as React from 'react';
import List from 'components/list/Container';
import Header from 'components/header/Component';
import Footer from 'components/footer/Component';
import Layout from 'components/layout/Component';

const ListPage: React.FC = () => (
  <div className="w100p h100p t0 l0">
    <Header />
    <Layout>
      <List />
    </Layout>
    <Footer />
  </div>
);

export default ListPage;
