import { connect } from 'react-redux';
import LandingPage from 'app/lp/Component';

const mapStateToProps = () => ({});

const mapDispatchToProps = () => ({});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(LandingPage);
