import React from 'react';
import 'assets/styles/theme.scss';

const wrapStyles = {
  display: 'flex',
  justifyContent: 'center',
  alignItems: 'center',
  minHeight: '100vh',
  width: '100%'
};

export const reactDecorator = story => (
    <div style={wrapStyles}>{story()}</div>
);
