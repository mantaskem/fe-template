# FE Project template

## Setup

```
$ yarn
```

## Start express development server

```
$ yarn start
```

## Build

Build for production

```
$ yarn build
```

## Start express production server

```
$ yarn server
```

## Storybook

Start storybook

```
$ yarn storybook
```

Build storybook

```
$ yarn build-storybook
```

## Analyze the Bundle Size

```
$ yarn analyze
```

# Structure of FE project

```
app/
	lp/
		Component.tsx # Have containers inside to represent landingPage
		Container.ts # Optional
components/
	registrationForm/
		submitButton/
			Component.tsx
			Container.ts
		Component.tsx # Registration form component
		Container.ts # All data which one is needed for Component should be provided from Container
	defaultButton/
		Container.ts # Uses button from ui/components/button/Component.tsx
features/
	registration/
		actions.ts # Actions creators only
		thunks.ts	# Thunks only
		selectors.ts # re-select, re-reselect or simple selectors which ones fetching data from state
		reducers.ts # reducers
		constants.ts # constants, enums and etc.
		thunks.test.ts
		actions.test.ts
		selectors.test.ts
ui/
	components/
		button/
			Component.tsx # Storybook component
utils/
	hof/
		withVisibility.tsx
	debounce.ts
	request.ts
```
